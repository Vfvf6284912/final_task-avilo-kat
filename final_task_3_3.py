import mysql.connector as mysql


db = mysql.connect(
    host="localhost",
    user="root",
    passwd="mypassword",
    database="car_sales",
    port=3307,
)
cursor = db.cursor()

# cursor.execute("SHOW TABLES")
# print(cursor.fetchall())

# Вывести список всех машин и их гос номер
query = "SELECT stamp,state_number FROM Vehicles"
cursor.execute(query)
print(cursor.fetchall())

# Вывести список машин и их гос номер, за исключением марки “Ниссан”
query = "SELECT stamp,state_number FROM Vehicles WHERE stamp != 'Nissan'"
cursor.execute(query)
print(cursor.fetchall())

# Вывести список машин и их гос номер, с пробегом от 100к до 150к км
query = "SELECT stamp,state_number FROM Vehicles WHERE mileage " \
        "BETWEEN 100000 AND 150000 ORDER BY mileage"
cursor.execute(query)
all_info = cursor.fetchall()
for one in all_info:
    print(one)


# *** Добавить пару машин в таблицу
query = "INSERT INTO Vehicles (type_machine, stamp, description, " \
        "state_number, rental_price, type_of_transm, mileage, drive) " \
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
values = [
    ('Hatchback', 'Mercedes', 'white', '1111', '25', 'Avto', '110000', 'rwt'),
    ('SUV ', 'Opel', 'black', '1234', '13', 'Avto', '125000', 'ful'),
]

cursor.executemany(query, values)
db.commit()

query = "SELECT stamp,state_number FROM Vehicles"
cursor.execute(query)
print(cursor.fetchall())

# Вывести кол-во зарегистрированных клиентов компании
query = "SELECT COUNT(*) as count FROM Buyers"
cursor.execute(query)
print(cursor.fetchall())

# *** Добавить поле “Скидка” в таблице “Покупатели”
cursor.execute("ALTER TABLE Buyers ADD COLUMN discount INT(3)")
cursor.execute("DESC Buyers")
print(cursor.fetchall())

# *** Заполнить столбец "Скидка"
cursor = db.cursor()
query = "UPDATE Buyers SET discount = '15' WHERE id = 1002"
cursor.execute(query)
db.commit()

# *** Заполнить столбец "Скидка"
cursor = db.cursor()
query = "UPDATE Buyers SET discount = '25' WHERE id = 1004"
cursor.execute(query)
db.commit()

# Вывести клиента с максимальной скидкой
cursor = db.cursor()
query = "SELECT MAX(discount) as max FROM Buyers"
cursor.execute(query)
print(cursor.fetchall())

# Найти общий пробег авто
cursor = db.cursor()
query = "SELECT SUM(mileage) as sum FROM Vehicles"
cursor.execute(query)
print(cursor.fetchall())
