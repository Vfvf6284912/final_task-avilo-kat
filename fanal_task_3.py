import docker
import time
import mysql.connector as mysql


mysql_image_name = 'mysql:latest'
client = docker.from_env()
list_of_images = client.images.list()

print('Trying to pull MySql image {name}'.format(name=mysql_image_name))
client.images.pull(mysql_image_name)

environment = ["MYSQL_ROOT_PASSWORD=mypassword"]
ports = {'3306/tcp': 3307}
container = client.containers.run(mysql_image_name,
                                  environment=environment,
                                  ports=ports, name='car_sales', detach=True)
time.sleep(50)

print('Container is working')

db = mysql.connect(
    port=3307,
    user="root",
    passwd="mypassword",
)

cursor = db.cursor()
cursor.execute('CREATE DATABASE car_sales')
cursor.execute('use car_sales')
cursor.execute("CREATE TABLE Vehicles (type_machine VARCHAR(10), "
               "stamp VARCHAR(50), description VARCHAR(255), "
               "state_number INT(20), rental_price INT(20), "
               "type_of_transm VARCHAR(10), mileage INT(6), "
               "drive ENUM('fwt', 'rwt', 'ful'))")


cursor.execute("CREATE TABLE Buyers (id INT(11), name VARCHAR(255), "
               "telephone INT(20), address VARCHAR(255))")

cursor.execute("CREATE TABLE Rental_history (id_rent INT(11), id INT(11), "
               "state_number INT(20), start_date DATE, end_date DATE)")

cursor.execute("SHOW TABLES")
print(cursor.fetchall())

query = "INSERT INTO Vehicles (type_machine, stamp, description, " \
        "state_number, rental_price, type_of_transm, mileage, drive) " \
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
values = [
    ('StatWagon', 'Acura', 'blue', '4256', '10', 'Mech', '100000', 'fwt'),
    ('Hatchback', 'AlfaRomeo', 'yellow', '3251', '15', 'Avto', '85000', 'rwt'),
    ('Minivan', 'Audi', 'red', '2365', '20', 'Avto', '35000', 'fwt'),
    ('SUV ', 'Nissan', 'green', '2332', '25', 'Avto', '55000', 'ful'),
]

cursor.executemany(query, values)
db.commit()
print(cursor.rowcount, "record inserted")

query = "INSERT INTO Buyers (id, name, telephone, address) " \
        "VALUES (%s, %s, %s, %s)"
values = [
    ('1001', 'Vasya Pupkin', '3392375', 'Minsk, Vost 35-64'),
    ('1002', 'Arnold Shwarz', '2325698', 'Minsk, Gor 56-121'),
    ('1003', 'Kolya Baskov', '1234578', 'Minsk, Dever 123-32'),
    ('1004', 'Don Ydon', '1236598', 'Joplyandiya 666'),
]

cursor.executemany(query, values)
db.commit()
print(cursor.rowcount, "record inserted")

query = "INSERT INTO Rental_history (id_rent, id, state_number, start_date, " \
        "end_date) VALUES (%s, %s, %s, %s, %s)"
values = [
    ('0001', '1002', '4256', '2021.01.01', '2021.01.11'),
    ('0002', '1004', '2332', '2021.01.05', '2021.01.15')
]

cursor.executemany(query, values)
db.commit()
print(cursor.rowcount, "record inserted")

cursor.close()
db.close()
container.stop()
print('Container stopped')
