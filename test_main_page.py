from pages.main_page import MainPage
from locators.main_page_locators import MainPageLocators

LINK = "http://automationpractice.com/index.php"


def test_user_can_login_page(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    browser.implicitly_wait(5)
    login_page = main_page.open_login_page()
    login_page.should_be_login_page()


def test_user_can_add_to_cart(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    dress_to_cart = main_page.is_element_present(
        *MainPageLocators.ITEM_IN_CART)
    dress_to_cart.click()
    browser.implicitly_wait(5)
    cart_page = main_page.is_element_present(
        *MainPageLocators.BUTT_PROCEED_CHECKOUT)
    cart_page.click()
    num_of_product = main_page.is_element_present(
        *MainPageLocators.NUM_PRODUCT_ON_CART)
    assert main_page.is_element_present(
        *MainPageLocators.BUTT_PROCEED_CHECKOUT_C) and num_of_product


def test_user_can_cart_page(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    browser.implicitly_wait(5)
    cart_page = main_page.open_cart_page()
    cart_page.should_be_empty_cart_page()
    assert main_page.is_element_present(*MainPageLocators.EMPTY_CART_PAGE)


def test_user_can_contact_page(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    browser.implicitly_wait(5)
    main_page.open_contact_page()
    assert main_page.is_element_present(*MainPageLocators.BUTT_SEND)


def test_user_can_take_more_info(browser):
    main_page = MainPage(browser, LINK)
    main_page.open(LINK)
    browser.implicitly_wait(5)
    main_page.button_More()
    assert main_page.is_element_present(*MainPageLocators.WISHLIST)
