from pages.base_page import BasePage
from locators.login_page_locators import LogPageLocators

changed_url = 'http://automationpractice.com/index.php?controller=' \
              'authentication&back=my-account'


class LoginPage(BasePage):

    def should_be_login_page(self):
        self.should_be_login_form()
        self.should_be_register_from()

    def should_be_login_form(self):
        self.is_element_present(*LogPageLocators.BUTTON_CREATE_ACC)

    def should_be_register_from(self):
        self.is_element_present(*LogPageLocators.LOGIN_BUTTON)

    def should_be_login_url(self):
        get_url = self.driver.current_url
        if get_url == changed_url:
            print(changed_url)


class LoginPageReg(BasePage):

    def enter_address(self, address):
        search_add = self.is_element_present(*LogPageLocators.EL_EMAIL)
        search_add.click()
        search_add.send_keys(address)
        return search_add

    def enter_pass(self, passwd):
        search_pass = self.is_element_present(*LogPageLocators.EL_PASSWD)
        search_pass.click()
        search_pass.send_keys(passwd)
        return search_pass

    def click_on_the_sign_in(self, browser):
        butt_can_be_click = self.is_element_present(
            *LogPageLocators.LOGIN_BUTTON)
        butt_can_be_click.click()
        browser.implicitly_wait(5)
        answer = self.is_element_present(*LogPageLocators.El_SIGN_IN)
        return answer


class LoginPageAcc(BasePage):

    def enter_address_create(self, address):
        search_add = self.is_element_present(*LogPageLocators.INPUT_E_ADDRESS)
        search_add.click()
        search_add.send_keys(address)
        return search_add

    def click_on_the_create_butt(self):
        search_add = self.is_element_present(
            *LogPageLocators.BUTTON_CREATE_ACC)
        search_add.click()
