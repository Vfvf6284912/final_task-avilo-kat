from pages.base_page import BasePage
from locators.cart_page_locators import CartPageLocators
import xml.etree.ElementTree as ET


CART_LINK = 'http://automationpractice.com/index.php?controller=order'

inf_price = ET.parse('FT.xml').getroot()


class CartPage(BasePage):
    def should_be_cart_page_not_empty(self):
        self.should_be_cart_form()
        self.should_be_process_checkout()
        self.should_be_cart_url()

    def should_be_empty_cart_page(self):
        self.is_element_present(*CartPageLocators.EMPTY_CART_PAGE)

    def should_be_cart_form(self):
        self.is_element_present(*CartPageLocators.BUTT_PLUS)

    def should_be_process_checkout(self):
        self.is_element_present(*CartPageLocators.BUTT_PROCEED_CHECKOUT)

    def should_be_cart_url(self):
        get_url = self.driver.current_url
        if get_url == CART_LINK:
            print(get_url)


class CartPageSumMin(BasePage):

    def add_to_cart(self, browser):
        dress_to_cart = self.is_element_present(
            *CartPageLocators.BUTT_ADD_CART)
        dress_to_cart.click()
        browser.implicitly_wait(5)
        cart_page = self.is_element_present(
            *CartPageLocators.BUTT_PROCEED_CHECKOUT_C)
        cart_page.click()
        browser.implicitly_wait(5)

    def total_price_change_if_plus(self, browser):
        plus = self.is_element_present(*CartPageLocators.BUTT_PLUS)
        total_price = self.is_element_present(*CartPageLocators.EL_TOTAL_PRISE)
        plus.click()
        browser.implicitly_wait(5)
        if total_price.text == inf_price[1][0].text:
            print('ok')
        elif total_price.text != inf_price[1][0].text:
            print("ERROR on total price")

    def total_price_change_if_minus(self, browser):
        minus = self.is_element_present(*CartPageLocators.BUTT_MINUS)
        total_price = self.is_element_present(*CartPageLocators.EL_TOTAL_PRISE)
        minus.click()
        browser.implicitly_wait(5)
        if total_price.text == inf_price[1][1].text:
            print('ok')
        elif total_price.text != inf_price[1][1].text:
            print("ERROR on total price")

    def if_button_delete_on_page(self):
        button_delete = self.is_element_present(*CartPageLocators.BUTT_DELETE)
        button_delete.click()
