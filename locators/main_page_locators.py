from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, '.login')
    ITEM_IN_CART = (By.XPATH, '//*[@id="homefeatured"]/li[1]/div/div[2]/'
                              'div[2]/a[1]/span')
    BUTT_PROCEED_CHECKOUT = (By.XPATH, '//*[@id="layer_cart"]/div[1]/'
                                       'div[2]/div[4]/a/span')
    BUTT_PROCEED_CHECKOUT_C = (By.CLASS_NAME, 'standard-checkout')
    NUM_PRODUCT_ON_CART = (By.XPATH, '//*[@id="header"]/div[3]/div/'
                                     'div/div[3]/div/a/span[1]')
    BUTT_SEND = (By.ID, 'submitMessage')
    EMPTY_CART_PAGE = (By.CLASS_NAME, 'alert-warning')
    WISHLIST = (By.ID, 'wishlist_button')


class MainPageFindLocators:
    ELE = (By.ID, 'contact-link')
    EL = (By.XPATH, '//*[@id="header"]/div[3]/div/div/div[3]/div/a')
    BUTT_MORE = (By.XPATH, '//*[@id="homefeatured"]/li[1]/div/'
                           'div[2]/div[2]/a[2]/span')
