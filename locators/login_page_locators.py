from selenium.webdriver.common.by import By


class LogPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, '.login')
    LOGIN_BUTTON = (By.ID, 'SubmitLogin')
    EL_EMAIL = (By.XPATH, '//*[@id="email"]')
    EL_PASSWD = (By.XPATH, '//*[@id="passwd"]')
    El_SIGN_IN = (By.XPATH, '//*[@id="center_column"]/div[1]/p')
    INPUT_E_ADDRESS = (By.ID, 'email_create')
    BUTTON_CREATE_ACC = (By.ID, 'SubmitCreate')
    INPUT_NAME = (By.ID, 'customer_firstname')
