from selenium.webdriver.common.by import By


class CartPageLocators:
    EMPTY_CART_PAGE = (By.CLASS_NAME, 'alert-warning')
    BUTT_PLUS = (By.ID, 'cart_quantity_up_1_1_0_0')
    BUTT_MINUS = (By.XPATH, '//*[@id="cart_quantity_down_1_1_0_0"]/span')
    BUTT_DELETE = (By.CLASS_NAME, 'icon-trash')
    BUTT_PROCEED_CHECKOUT = (By.CLASS_NAME, 'standard-checkout')
    BUTT_ADD_CART = (By.XPATH, '//*[@id="homefeatured"]/li[1]/div/div[2]/'
                               'div[2]/a[1]/span')
    BUTT_PROCEED_CHECKOUT_C = (By.XPATH, '//*[@id="layer_cart"]/'
                                         'div[1]/div[2]/div[4]/a/span')
    EL_UNIT_PRISE = (By.ID, 'product_price_1_1_0')
    EL_TOTAL_PRISE = (By.ID, 'total_product_price_1_1_0')
    EL_TOTAL_PRODUCT = (By.ID, 'total_product')


class CartPageLocatorsTest:
    EL_QTY = (By.CLASS_NAME, 'cart_quantity_input')
    EL_AFTER_DEL = (By.CLASS_NAME, 'alert-warning')
