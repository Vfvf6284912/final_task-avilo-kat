from pages.cart_page import CartPageSumMin
from locators.cart_page_locators import CartPageLocatorsTest, CartPageLocators
import xml.etree.ElementTree as ET


CART_LINK = 'http://automationpractice.com/index.php?controller=order'
LINK = "http://automationpractice.com/index.php"

inf_price = ET.parse('FT.xml').getroot()


def test_user_can_cart_page(browser):
    main_page = CartPageSumMin(browser, LINK)
    main_page.open(LINK)
    main_page.add_to_cart(browser)


def test_button_plus(browser):
    main_page = CartPageSumMin(browser, LINK)
    main_page.open(LINK)
    main_page.add_to_cart(browser)
    main_page.total_price_change_if_plus(browser)
    assert main_page.is_element_present(*CartPageLocatorsTest.EL_QTY).text \
           != '1'


def test_button_minus(browser):
    main_page = CartPageSumMin(browser, LINK)
    main_page.open(LINK)
    main_page.add_to_cart(browser)
    main_page.total_price_change_if_plus(browser)
    main_page.total_price_change_if_minus(browser)
    assert main_page.is_element_present(*CartPageLocators.EL_TOTAL_PRISE).text\
           == inf_price[1][1].text


def test_button_delete(browser):
    main_page = CartPageSumMin(browser, LINK)
    main_page.open(LINK)
    main_page.add_to_cart(browser)
    main_page.if_button_delete_on_page()
    assert main_page.is_element_present(*CartPageLocatorsTest.EL_AFTER_DEL)
