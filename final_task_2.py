import requests
import json

# dict_file = [{'myToken' : '8747837e8629316f91d80d251b827933f3b7cee19d5e8aede'
#                           '17937f8593061fd'},
# {'myUrl' : 'https://gorest.co.in/public-api/users/8'}]
# # созаем файл
# with open(r'C:\Users\Kat\PycharmProjects\pythonProject\FT.json', 'w') \
#         as file:
#     documents = json.dump(dict_file, file)

# посмотреть кого можно удалять, менять...
response = requests.get("https://gorest.co.in/public-api/users")
print(response.text)


def test_get_json():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        inf_url_user = text[1]['myUrlUser']
        response_get_user = requests.get(inf_url_user)
        items = response_get_user.json()
        # print(response_get_user.status_code)
        # print(response_get_user.text)
        assert response_get_user.status_code == 200 \
               and items['data']['id'] != ''


def test_post_json():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        inf_user = text[6]['infUser']
        inf_token = text[0]['myToken']
        inf_url = text[2]['myUrl']
        headers = {"Authorization": f"Bearer {inf_token}"}
        response = requests.post(inf_url, headers=headers, data=inf_user)
        json_response = response.json()
        # print(json_response['code'])
        # print(response.status_code)
        assert response.status_code == 200 and json_response['code'] == 422


def test_put_json():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        inf_user_change = text[7]['infUserChange']
        inf_token = text[0]['myToken']
        url_put = text[3]['urlDelPut']
        headers = {"Authorization": f"Bearer {inf_token}"}
        response = requests.put(url_put, headers=headers, data=inf_user_change)
        json_response = response.json()
        # print(json_response)
        # print(response.status_code)
        assert response.status_code == 200 and json_response['code'] == 200


def test_delete_json():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        inf_token = text[0]['myToken']
        url_put = text[3]['urlDelPut']
        headers = {"Authorization": f"Bearer {inf_token}"}
        response = requests.delete(url_put, headers=headers)
        json_response = response.json()
        # print(json_response)
        # print(response.status_code)
        assert response.status_code == 200 and json_response['code'] == 204


def test_negative_auth_token():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        auth_token = text[5]['tokenNot']
        inf_url = text[2]['myUrl']
        inf_user = (text[6]['infUser'])
        headers = {"Authorization": f"Bearer {auth_token}"}
        response = requests.post(inf_url, headers=headers, data=inf_user)
        assert response.status_code == 200, \
            f"Failed to request. Response: {response}"


def test_negative_user():
    with open('FT.json', 'r', encoding='utf-8') as f:
        text = json.load(f)
        url_user = text[5]['urlNotUser']
        response = requests.get(url_user)
        assert response.status_code == 200, \
            f"Failed to request. Response: {response}"
