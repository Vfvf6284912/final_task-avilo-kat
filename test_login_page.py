from pages.login_page import LoginPage, LoginPageReg, LoginPageAcc
from locators.login_page_locators import LogPageLocators
import xml.etree.ElementTree as ET


inf_for_log = ET.parse('FT.xml').getroot()

LOG_LINK = 'http://automationpractice.com/index.php?controller=' \
           'authentication&back=my-account'


def test_in_login_page(browser):
    log_page = LoginPage(browser, LOG_LINK)
    log_page.open(LOG_LINK)
    log_page.should_be_login_page()
    log_page.should_be_login_url()
    assert log_page.is_element_present(*LogPageLocators.BUTTON_CREATE_ACC)


def test_if_client_registered(browser):
    reg_log_page = LoginPageReg(browser, LOG_LINK)
    reg_log_page.open(LOG_LINK)
    reg_log_page.enter_address(inf_for_log[0][0].text)
    reg_log_page.enter_pass(inf_for_log[0][1].text)
    answer = reg_log_page.click_on_the_sign_in()
    assert answer.text == 'There is 1 error'


def test_if_client_create_an_account(browser):
    reg_log_page = LoginPageAcc(browser, LOG_LINK)
    reg_log_page.open(LOG_LINK)
    reg_log_page.enter_address_create(inf_for_log[0][0].text)
    reg_log_page.click_on_the_create_butt()
    assert reg_log_page.is_element_present(*LogPageLocators.INPUT_NAME)
